<?php
namespace Morsmordre\Api\antstory;

use Morsmordre\Api\ApiClient;
use app\common\dto\UserDTO;

/**
 * Class Point
 *
 * @author sunanzhi <sunanzhi@hotmail.com>
 */
class Point
{
    /**
     * 添加积分
     *
     * @param integer $userId 用户id
     * @param integer $category 0：签到3积分，1：评论2积分，2：分享5积分
     * @return boolean
     * 
     * @author sunanzhi <sunanzhi@sunanzhi.com>
     */
    public function addPoint(int $userId, int $category):bool
    {
        return ApiClient::request('antstory/Point', __FUNCTION__, true, 'bool', $userId, $category);
    }
}
