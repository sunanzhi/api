<?php
namespace Morsmordre\Api\demo;

use Morsmordre\Api\ApiClient;

/**
 * class Test
 * 
 * @author sunanzhi <sunanzhi@hotmail.com>
 */
class Test
{
    /**
     * 测试不同模块调用案例
     *
     * @param string $username 用户名称
     * @param integer $action 行为
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.3.30
     */
    public function test(string $username, int $action):array
    {
        return ApiClient::request('demo/Test', __FUNCTION__, $username, $action);
    }
}