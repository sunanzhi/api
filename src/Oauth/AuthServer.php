<?php
namespace Morsmordre\Api\oauth;

use Morsmordre\Api\ApiClient;

/**
 * class AuthServer
 * 
 * @author sunanzhi <sunanzhi@hotmail.com>
 */
class AuthServer
{
    public function getAccessToken(): array
    {
        return ApiClient::request('oauth/AuthServer', __FUNCTION__, false, 'array');
    }
}