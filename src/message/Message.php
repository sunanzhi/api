<?php
namespace Morsmordre\Api\message;

use Morsmordre\Api\ApiClient;

/**
 * class Message
 * 
 * @author sunanzhi <sunanzhi@hotmail.com>
 */
class Message
{
    /**
     * 内容违规检测微信
     *
     * @param string $content 内容
     * @param string $appId 小程序appId
     * @return boolean
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     */
    public function contentCheckForApplet(string $content, string $appId):bool
    {
        return ApiClient::request('message/Message', __FUNCTION__, false, 'bool', $content, $appId);
    }
}