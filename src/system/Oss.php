<?php
namespace Morsmordre\Api\system;

use Morsmordre\Api\ApiClient;

/**
 * Class Oss
 *
 * @author sunanzhi <sunanzhi@hotmail.com>
 */
class Oss
{
    /**
     * 通过url上传图片
     *
     * @param string $url url
     * @param string $filePath 文件路径
     * @param string $suffix 文件类型
     * @return string
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     */
    public function uploadByUrl(string $url, string $filePath, string $suffix, string $filename = ''):string
    {
        return ApiClient::request('system/Oss', __FUNCTION__, false, 'string', $url, $filePath, $suffix, $filename);
    }

    /**
     * 字符串上传 文件流
     *
     * @param string $filename 文件路径
     * @param string $suffix 文件类型
     * @param string $content 内容
     * @param string $filename 文件名
     * @return string
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2018.7.26
     */
    public function putObject(string $filePath, string $suffix, string $content, string $filename = ''):string
    {
        return ApiClient::request('system/Oss', __FUNCTION__, false, 'string', $filePath, $suffix, $content, $filename);
    }

    /**
     * 下载小程序码链接
     *
     * @param string $appId appId
     * @param array $postData 请求数据
     * @param array $extra 额外拓展
     * @return string
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.9.26
     */
    public function downloadQRCode(string $appId, array $postData, array $extra):string
    {
        return ApiClient::request('system/Oss', __FUNCTION__, false, 'string', $appId, $postData, $extra);
    }
}
