<?php
namespace Morsmordre\Api\user;

use Morsmordre\Api\ApiClient;

/**
 * Class User
 *
 * @author sunanzhi <sunanzhi@hotmail.com>
 */
class User
{
    /**
     * getUserByUsernameAndPassword
     *
     * @param string $username
     * @param string $password
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     */
    public function getUserByUsernameAndPassword(string $username, string $password): array
    {
        return ApiClient::request('user/User', __FUNCTION__, false, 'array', $username, $password);
    }

    /**
     * 获取用户信息
     *
     * @param array $userIds 用户id
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.9.6
     */
    public function getUserByIds(array $userIds): array
    {
        return ApiClient::request('user/User', __FUNCTION__, false, 'array', $userIds);
    }
}
