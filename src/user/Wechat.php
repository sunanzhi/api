<?php
namespace Morsmordre\Api\user;

use Morsmordre\Api\ApiClient;

/**
 * Class Wechat
 *
 * @author sunanzhi <sunanzhi@hotmail.com>
 */
class Wechat
{
    /**
     * 获取accessToken
     *
     * @param string $appId 微信appId
     * @return string
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 219.9.16
     */
    public function getAccessToken(string $appId): string
    {
        return ApiClient::request('user/Wechat', __FUNCTION__, false, 'string', $appId);
    }

}
