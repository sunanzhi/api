<?php
namespace Morsmordre\Api\user;

use Morsmordre\Api\ApiClient;
use app\common\dto\UserDTO;

/**
 * Class Oauth
 *
 * @author sunanzhi <sunanzhi@hotmail.com>
 */
class Oauth
{
    /**
     * getUserByPasswordGrant
     *
     * @param integer $grantType 
     * @param array $params 请求参数
     * @param array $extra 额外参数
     * @return UserDTO
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     */
    public function getUserByPasswordGrant(int $grantType, array $params, array $extra): UserDTO
    {
        return ApiClient::request('user/Oauth', __FUNCTION__, false, 'UserDTO', $grantType, $params, $extra);
    }
}
